playBasic:
play:
    ld hl,soundEffectsData    ;address of sound effects data
    di
    push ix
    push iy

    ld b,0
    ld c,a
    add hl,bc
    add hl,bc
    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    push hl
    pop ix            ;put it into ix
   
    ld a,(23624)    ;get border color from BASIC vars to keep it unchanged
    rra
    rra
    rra
    and 7
    ld (sfxRoutineToneborder+1),a
    ld (sfxRoutineNoiseborder+1),a
   

readData:
    ld a,(ix+0)        ;read block type
    or a
    jr nz,readDatasound
    pop iy
    ei
    jp SoundEnd

readDatasound:
    ld c,(ix+1)        ;read duration 1
    ld b,(ix+2)
    ld e,(ix+3)        ;read duration 2
    ld d,(ix+4)
    push de
    pop iy

    dec a
    jr nz,sfxRoutineNoise



;this routine generate tone with many parameters
sfxRoutineTone:
    ld e,(ix+5)        ;freq
    ld d,(ix+6)
    ld a,(ix+9)        ;duty
    ld (sfxRoutineToneduty+1),a
    ld hl,0
   
sfxRoutineTonel0:
    push bc
    push iy
    pop bc
sfxRoutineTonel1:
    add hl,de
    ld a,h
sfxRoutineToneduty:
    cp 0
    sbc a,a
    and 16
sfxRoutineToneborder:
    or 0
    out ($fe),a
#line 78               
    dec bc
    ld a,b
    or c
    jr nz,sfxRoutineTonel1

    ld a,(sfxRoutineToneduty+1)
    add a,(ix+10)    ;duty change
    ld (sfxRoutineToneduty+1),a

    ld c,(ix+7)        ;slide
    ld b,(ix+8)
    ex de,hl
    add hl,bc
    ex de,hl

    pop bc
    dec bc
    ld a,b
    or c
    jr nz,sfxRoutineTonel0

    ld c,11
nextData:
    add ix,bc        ;skip to the next block
    jr readData



;this routine generate noise with two parameters
sfxRoutineNoise:
    ld e,(ix+5)        ;pitch

    ld d,1
    ld h,d
    ld l,d
sfxRoutineNoisel0:
    push bc
    push iy
    pop bc
sfxRoutineNoisel1:
    ld a,(hl)
    and 16
sfxRoutineNoiseborder:
    or 0
    out ($fe),a
    dec d
    jr nz,sfxRoutineNoisel2
    ld d,e
    inc hl
    ld a,h
    and $1f
    ld h,a
sfxRoutineNoisel2:
    dec bc
    ld a,b
    or c
    jr nz,sfxRoutineNoisel1

    ld a,e
    add a,(ix+6)    ;slide
    ld e,a

    pop bc
    dec bc
    ld a,b
    or c
    jr nz,sfxRoutineNoisel0

    ld c,7
    jr nextData