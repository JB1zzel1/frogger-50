#include "doubleSizePrint.bas"
#include <screen.bas>
POKE UINTEGER 23675,@graphicsbank
poke uInteger 23606,@typeface-256
DIM Speed1, Speed2, Speed3 as UINTEGER
DIM b, a, y2, c, hi, lives, score, home, x1, y1, x2, SpeedControl
let SpeedControl1 = 0 : let hi = 0
Print at 20,11; ink 1 ; paper 6 ;flash 1; "PRESS A KEY"
'randomize usr val "60000"
GO TO PrintMenu

DieFall:

BEEP .01,b-a
PRINT OVER 1; PAPER 8; INK 8;AT a,y2;"\g": RETURN ' frog falls down as sound plays and you die!'

PrintMenu:

paper 5 : border 5 : CLS : pause 2
doubleSizePrint (3,6,"FROGGER 50")
PRINT AT 11,3;"Do you want instructions?";AT 13,11;"(y)es";AT 15,11;"(n)o"
pause 0
IF INKEY$="y" THEN GO TO Object:END IF
GO TO StarGame

Object: 
CLS 
doubleSizePrint (1,10,"OBJECT")
print at 4,0; "To guide a \g across a road and ariver,avoiding \a\b \c\d\e \f \q\r\r\s \t\u." : Print "A \o\p patrols the central island."
POKE UINTEGER 23675,@graphicsbank0
PRINT "There are 4 HOMES to be filled. i.e. gaps in top WALL \a\a\a \a\a\a"
POKE UINTEGER 23675,@graphicsbank
PRINT "Once all 4 HOMES are filled the speed will increase,an extra \o\p will be added and the HOMES will empty."
PRINT AT 18,9;"Press any key.": PAUSE 0
CLS : PRINT AT 7,11;"CONTROLS"
PRINT "      ^               < >"
PRINT FLASH 1;AT 11,6;"1"; FLASH 0;" 2 3 4 5 6 7 8 "; FLASH 1;"9"; FLASH 0;" "; FLASH 1;"0"
PRINT AT 18,5;"Press any key to PLAY": PAUSE 0
StarGame:
BRIGHT 1: PAPER 5: BORDER 5: CLS
PrintSpider: 
PRINT PAPER 4;AT 10,0;"               \o\p               " : REM the spider in the middle of the screen
LET lives=9: LET score=0: LET home=0 : REM set initial values, home is 0 frogs rescued
let SpeedControl1 = 0

PrintScreen: 

POKE UINTEGER 23675,@graphicsbank0
PRINT AT 0,0; PAPER 2; ink 6 ;"\a\a\a\a";" "; ink 6 ; PAPER 2;"\a\a\a\a\a\a"; ink 4;" "; ink 6 ; PAPER 2;"\a\a\a\a\a\a\a"; ink 5;" ";  ink 6 ;PAPER 2;"\a\a\a\a\a\a\a"; ink 1;" ";  ink 6 ;PAPER 2;"\a\a\a\a"
POKE UINTEGER 23675,@graphicsbank
IF home<>0 THEN GO TO DetectCol:END IF ' come back here and clear homes after level complete - but dont re draw the whole screen'
PRINT PAPER 4; INK 5; at 1,0 ;"\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\.."
PRINT  at 2,0; " \q\r\r\r\s   \q\r\r\r\s    \q\r\r\s   \q\r\r\s   "
PRINT INK 7; at 3,0 ;"    \m\m\m     \m\m\m\m\m     \m\m\m      \m"
PRINT INK 2; at 4,0 ;"  \t\u     \t\u     \t\u   \t\u      \t\u "
PRINT INK 7; at 5,0 ;"\m\m   \m      \m\m\m\m\m        \m\m\m    "
PRINT INK 1; at 6,0 ;"\r\s     \q\r\r\s      \q\r\r\r\r\s     \q\r\r\r"
PRINT INK 7; at 7,0 ;"   \m\m  \m\m\m\m\m  \m\m       \m\m\m      "
PRINT  at 8,0 ;"\u         \t\u     \t\u      \t\u    \t"
PRINT PAPER 4; at 9,0 ;"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
PRINT PAPER 0; INK 7;AT 11,0;"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
PRINT PAPER 0; INK 3; at 12,0 ;" \a\b \f  \a\b  \c\d\c\d\e \f \f   \c\d\e      "
POKE UINTEGER 23675,@graphicsbank0
PRINT PAPER 0; INK 7; at 13,0 ;"\b \b \b \b \b \b \b \b \b \b \b \b \b \b \b \b "
POKE UINTEGER 23675,@graphicsbank
PRINT PAPER 0; INK 7; at 13,0 ;"- - - - - - - - - - - - - - - - "
PRINT PAPER 0; INK 5; at 14,0 ;" \a\b    \a\b    \a\b  \a\b        \a\b   "
POKE UINTEGER 23675,@graphicsbank0
PRINT PAPER 0; INK 7; at 15,0 ;"\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
POKE UINTEGER 23675,@graphicsbank
PRINT PAPER 0; INK 4; at 16,0 ;"\i     \h\i   \h\i       \h\i         \h"
PRINT PAPER 0; INK 7; at 17,0 ;"- - - - - - - - - - - - - - - - "
PRINT PAPER 0; INK 6; at 18,0 ;"\k\l   \h\i  \h\i \f  \f  \j\k\l\k\l   \f    \j"
PRINT PAPER 4; at 19,0 ;"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
PRINT PAPER 4; at 20,0 ;"                                "
PRINT PAPER 1; INK 7; at 21,0 ;" SCORE ";AT 21,11;"FROGS"; PAPER 5; INK 0;lives; PAPER 1; INK 7;" HI-SCORE "
PRINT; ink 0; AT 21,27;hi

DetectCol:

LET x1=20: LET y1=16: LET x2=x1: LET y2=y1 ' starting location of the frog?

RemoveFrog:

PRINT PAPER 8; INK 8;AT x1,y1;" "

Scroll:

scroll()
if SpeedControl1 > 0 then SpeedC1() : END IF
if SpeedControl1 > 1 then SpeedC2() : END IF
if SpeedControl1 = 3 then SpeedC3() : END IF
IF  SCREEN$(x2,y2)=" " THEN GO TO FrogHome:END IF
LET a=x2: FOR b=25 TO 35: GO SUB DieFall: GO SUB DieFall: NEXT b ' play music on death, different dependant on how high you got'
FOR a=x2 TO 20 STEP 2: GO SUB DieFall: GO SUB DieFall: NEXT a
LET lives=lives-1: PRINT AT 21,16;lives ' remove a life and update user display'
LET x2=20
IF lives<>0 THEN GO TO Scroll:END IF
IF hi>score THEN GO TO GameOver:END IF
LET hi=score: PRINT AT 21,27;hi

GameOver:

PRINT FLASH 1; PAPER 7;AT 12,0;"           GAME  OVER           "
PRINT AT 14,0;"  Another game ? (y)es  (n)o    "
randomize usr val "60000"

Again: 

IF INKEY$="n" THEN go to PrintMenu:END IF : REM resets the spectrum
IF INKEY$<>"y" THEN GO TO Again:END IF
PRINT PAPER 5;AT 21,7;"    ": GO TO PrintSpider

FrogHome:

IF x2<>0 THEN GO TO PrintFrog:END IF
POKE UINTEGER 23675,@graphicsbank0
PRINT PAPER 8; INK 8;AT x1,y1;" ": PRINT; ink 6; paper 2; AT x2,y2;"\c" ' if get home put a frog in the home'
POKE UINTEGER 23675,@graphicsbank
RESTORE Win

Win:

sound(1)
LET home=home+1: LET score=score+50: PRINT AT 21,7;score
IF home/4<>INT (home/4) THEN GO TO DetectCol:END IF ' have you completed the game?'
IF home=4 THEN let SpeedControl1 = 1:END IF
IF home=8 THEN let SpeedControl1 = 2:END IF
IF home=12 THEN let SpeedControl1 = 3:END IF
IF home>36 THEN GO TO PrintScreen:END IF ' limit the number of extra spiders'
LET a=RND*31 : 

ExtraSpider:  

LET a=a+1
IF a>31 THEN LET a=0:END IF : 
IF  SCREEN$ (10,a)="" THEN GO TO ExtraSpider:END IF : 
IF  SCREEN$ (10,a+1)="" THEN GO TO ExtraSpider:END IF ' find a place for the extra spider can fit'
PRINT PAPER 4;AT 10,a;"\o\p" : rem print the spider
RESTORE Win ': FOR a=1 TO 8: READ b,c: BEEP b,c: NEXT a
sound(2) ' oh no sound'
GO TO PrintScreen

PrintFrog: 

PRINT PAPER 8; INK 8;AT x2,y2;"\g" : rem draw the frog
pause 3
LET x1=x2: LET y1=y2
IF INKEY$<>"1" THEN GO TO KeyInput:END IF
BEEP .001,33
LET x2=x2-2: LET score=score+5: PRINT AT 21,7;score

KeyInput: 

LET y2=y2+(INKEY$="0" AND y2<>31)-(INKEY$="9" AND y2<>0)
GO TO RemoveFrog

graphicsbank0:

ASM
defb 022H, 0FFH, 088H, 0FFH, 022H, 0FFH, 088H, 0FFH ; a - wall
Defb 000H, 000H, 000H, 0FFH, 014H, 000H, 000H, 000H ; b road centre 
defb 018H, 03CH, 07EH, 07EH, 07EH, 07CH, 07EH, 07EH ; c Door
END ASM

graphicsbank:

ASM
defb 00Fh,012h,022h,07Fh,0FFh,0FFh,028h,010h;a
defb 080h,040h,020h,0FEh,0FEh,0FFh,028h,010h;b
defb 07Fh,07Fh,07Fh,07Fh,07Fh,0FFh,015h,08h      ;c
defb 0FEh,0FEh,0FEh,0FEh,0FFh,0FFh,040h,080h ; d
defb 000h,0F8h,0C4h,0C4h,0FEh,0FEh,028h,010h
defb 018h,018h,024h,07Eh,03Ch,05Ah,0A5h,042h
defb 0E7H, 0A5H, 0FFH, 081H, 07EH, 07EH, 03CH, 0E7H; g frog
defb 001h,02h,04h,07Fh,07Fh,0FFh,014h,08h
defb 0F0h,048h,044h,0FEh,0FFh,0FFh,014h,08h
defb 000h,01Fh,023h,023h,07Fh,07Fh,014h,08h
defb 07Fh,07Fh,07Fh,07Fh,0FFh,0FFh,02h,01h
defb 0FEh,0FEh,0FEh,0FEh,0FEh,0FFh,0A8h,010h
defb 010h,029h,0C7h,00h,026h,00h,00h,00h
defb 000H, 018H, 0FFH, 018H, 0FFH, 018H, 018H, 018H; n fence
defb 000h,022h,055h,08Fh,097h,0A3h,0A0h,00h
defb 000h,044h,0AAh,0F1h,0E9h,0C5h,05h,00h
defb 010h,010h,010h,0FEh,03Fh,01Fh,0Fh,07h
defb 000h,00h,00h,00h,01Eh,0FFh,0FFh,0FFh
defb 060h,07Ch,054h,078h,07Fh,0FFh,0FEh,0FCh
defb 000h,00h,03h,02h,0Fh,03Fh,0FFh,00h
defb 006h,0Ch,098h,0F0h,0E0h,055h,0FFh,00h
End ASM

typeface:

ASM

  ; Nibbles Bold font by DamienG https://damieng.com
  defb 000h,000h,000h,000h,000h,000h,000h,000h ;  
  defb 000h,038h,038h,038h,000h,038h,000h,000h ; !
  defb 000h,06ch,06ch,06ch,000h,000h,000h,000h ; "
  defb 000h,06ch,0feh,06ch,0feh,06ch,000h,000h ; #
  defb 000h,018h,07eh,078h,01eh,07eh,018h,000h ; $
  defb 000h,072h,074h,018h,02eh,04eh,000h,000h ; %
  defb 000h,07eh,070h,03eh,06ch,07eh,000h,000h ; 0
  defb 000h,018h,018h,018h,000h,000h,000h,000h ; '
  defb 000h,03ch,070h,070h,07ch,03ch,000h,000h ; (
  defb 000h,03ch,00eh,00eh,03eh,03ch,000h,000h ; )
  defb 000h,018h,07eh,03ch,07eh,018h,000h,000h ; *
  defb 000h,000h,038h,0feh,038h,000h,000h,000h ; +
  defb 000h,000h,000h,000h,018h,018h,018h,000h ; h,
  defb 000h,000h,000h,07ch,000h,000h,000h,000h ; -
  defb 000h,000h,000h,000h,030h,030h,000h,000h ; .
  defb 000h,00ch,01ch,038h,070h,060h,000h,000h ; /
  defb 000h,07eh,076h,076h,07eh,07eh,000h,000h ; 0
  defb 000h,078h,018h,018h,07eh,07eh,000h,000h ; 1
  defb 000h,07eh,00eh,038h,07eh,07eh,000h,000h ; 2
  defb 000h,07eh,00eh,03ch,00eh,07eh,000h,000h ; 3
  defb 000h,03ch,05ch,07eh,07eh,01ch,000h,000h ; 4
  defb 000h,07eh,070h,07eh,00eh,07ch,000h,000h ; 5
  defb 000h,07eh,070h,07eh,076h,07eh,000h,000h ; 6
  defb 000h,07eh,00eh,00eh,01ch,01ch,000h,000h ; 7
  defb 000h,07eh,076h,03ch,06eh,07eh,000h,000h ; 8
  defb 000h,07eh,06eh,07eh,00eh,07eh,000h,000h ; 9
  defb 000h,000h,018h,000h,018h,018h,000h,000h ; :
  defb 000h,000h,018h,000h,018h,018h,018h,000h ;h ;
  defb 000h,01ch,038h,070h,038h,01ch,000h,000h ; <
  defb 000h,000h,07ch,000h,07ch,000h,000h,000h ; =
  defb 000h,038h,01ch,00eh,01ch,038h,000h,000h ; >
  defb 000h,07eh,00eh,03ch,000h,038h,000h,000h ; ?
  defb 000h,07eh,06eh,06eh,060h,07eh,000h,000h ; @
  defb 000h,07eh,06eh,07eh,06eh,06eh,000h,000h ; A
  defb 000h,07eh,076h,07ch,076h,07eh,000h,000h ; B
  defb 000h,07eh,070h,070h,07eh,07eh,000h,000h ; C
  defb 000h,07ch,076h,076h,07eh,07eh,000h,000h ; D
  defb 000h,07eh,070h,07eh,078h,07eh,000h,000h ; E
  defb 000h,07eh,070h,07eh,070h,070h,000h,000h ; F
  defb 000h,07eh,070h,076h,07eh,07eh,000h,000h ; G
  defb 000h,076h,076h,07eh,076h,076h,000h,000h ; H
  defb 000h,038h,038h,038h,038h,038h,000h,000h ; I
  defb 000h,00eh,00eh,06eh,07eh,07eh,000h,000h ; J
  defb 000h,076h,076h,07ch,07eh,076h,000h,000h ; K
  defb 000h,070h,070h,070h,07eh,07eh,000h,000h ; L
  defb 000h,0c6h,0eeh,0feh,0feh,0d6h,000h,000h ; M
  defb 000h,066h,076h,07eh,07eh,076h,000h,000h ; N
  defb 000h,07eh,076h,076h,07eh,07eh,000h,000h ; O
  defb 000h,07eh,076h,07eh,07eh,070h,000h,000h ; P
  defb 000h,07eh,076h,076h,07eh,07eh,00eh,000h ; Q
  defb 000h,07eh,076h,07ch,07eh,076h,000h,000h ; R
  defb 000h,07eh,070h,07eh,00eh,07eh,000h,000h ; S
  defb 000h,0feh,038h,038h,038h,038h,000h,000h ; T
  defb 000h,076h,076h,076h,07eh,07eh,000h,000h ; U
  defb 000h,076h,076h,076h,07ch,078h,000h,000h ; V
  defb 000h,0c6h,0d6h,0feh,0feh,0eeh,000h,000h ; W
  defb 000h,076h,07eh,03ch,07eh,06eh,000h,000h ; X
  defb 000h,076h,076h,07eh,00eh,07eh,000h,000h ; Y
  defb 000h,07eh,01ch,038h,07eh,07eh,000h,000h ; Z
  defb 000h,07eh,070h,070h,07eh,07eh,000h,000h ; [
  defb 000h,060h,070h,038h,01ch,00ch,000h,000h ; \
  defb 000h,07eh,00eh,00eh,07eh,07eh,000h,000h ; ]
  defb 000h,018h,03ch,07eh,03ch,03ch,000h,000h ; ^
  defb 000h,000h,000h,000h,000h,000h,07eh,000h ; _
  defb 000h,07eh,072h,07ch,070h,07eh,000h,000h ; £
  defb 000h,000h,07eh,06eh,07eh,076h,000h,000h ; a
  defb 000h,070h,07eh,076h,07eh,07eh,000h,000h ; b
  defb 000h,000h,07eh,060h,07eh,07eh,000h,000h ; c
  defb 000h,00eh,07eh,06eh,07eh,07eh,000h,000h ; d
  defb 000h,000h,07eh,076h,07ch,07eh,000h,000h ; e
  defb 000h,01eh,01ch,03eh,01ch,01ch,000h,000h ; f
  defb 000h,000h,07eh,06eh,07eh,00eh,07eh,000h ; g
  defb 000h,070h,07eh,076h,076h,076h,000h,000h ; h
  defb 000h,038h,000h,038h,038h,038h,000h,000h ; i
  defb 000h,01ch,000h,01ch,01ch,01ch,03ch,000h ; j
  defb 000h,070h,076h,07ch,076h,076h,000h,000h ; k
  defb 000h,03ch,01ch,01ch,01ch,01ch,000h,000h ; l
  defb 000h,000h,0eeh,0feh,0feh,0d6h,000h,000h ; m
  defb 000h,000h,07eh,076h,076h,076h,000h,000h ; n
  defb 000h,000h,07eh,076h,07eh,07eh,000h,000h ; o
  defb 000h,000h,07eh,076h,07eh,07eh,070h,000h ; p
  defb 000h,000h,07eh,06eh,07eh,07eh,00eh,000h ; q
  defb 000h,000h,07eh,076h,070h,070h,000h,000h ; r
  defb 000h,000h,07eh,078h,01eh,07eh,000h,000h ; s
  defb 000h,038h,07eh,038h,03eh,03eh,000h,000h ; t
  defb 000h,000h,076h,076h,076h,07eh,000h,000h ; u
  defb 000h,000h,076h,076h,07ch,078h,000h,000h ; v
  defb 000h,000h,0d6h,0feh,0feh,0eeh,000h,000h ; w
  defb 000h,000h,076h,03ch,07eh,06eh,000h,000h ; x
  defb 000h,000h,06eh,06eh,07eh,00eh,07eh,000h ; y
  defb 000h,000h,07eh,01eh,078h,07eh,000h,000h ; z
  defb 000h,01eh,01ch,078h,01ch,01eh,000h,000h ; {
  defb 000h,038h,038h,038h,038h,038h,000h,000h ; |
  defb 000h,078h,038h,01eh,038h,078h,000h,000h ; }
  defb 000h,076h,06eh,000h,000h,000h,000h,000h ; ~
  defb 07fh,041h,03eh,030h,03eh,03eh,041h,07fh ; ©


END ASM

function fastcall scroll () 

ASM

JR START ; Jump to start of program
  ; Scroll right routine starts here
ScrRight:
  LD C,$08          ; put 8 in C this whole section is looped 8 times
L7DF6:
  PUSH HL           ; store on the stack whatever is in HL already 
  LD DE,$001F       ; load de with 31
  ADD HL,DE         ; Add 31 to what is in HL
  LD A,(HL)         ; load A with what is in the location of HL
  SBC HL,DE         ; subtract de with carry (31) from HL
  RRA               ; Rotate right accumulator  
  
    LD B,$20          ; Load B with 32        - IE loop this next bit 32 times
L7E01:                
  LD A,(HL)         ; load a with what is at HL location
  RRA               ; rotate right with carry
  LD (HL),A         ; store at location HL what is in A
  INC HL            ; HL=HL+1
  DJNZ L7E01        ; Let b=b-1. If B > 0 loop back to L7E01

  POP HL            ; restore HL to what it was at l7df6
  INC H             ; H=H+1
  DEC C             ; C (which is 8) = C-1
  JR NZ,L7DF6       ; If C > 0 then go to L7df6
  RET               ; else return
  ; Scroll left routine starts here
ScrLeft:
  LD C,$08          ; load c with 8 (setting up a loop)
L7E0F:
  XOR A             ; set A to zero
  PUSH HL           ; put HL on the stack, from the call earlier HL is right edge, 3 attr down: #405F
  LD DE,$001F       ; load de 31 (number of attributes in a row on the screen)
  SBC HL,DE         ; subtract DE from HL with carry  4005-001f = left edge, 3 attr down: #4040F
  LD A,(HL)         ; load A with what's at the HL location - ie left edge 3 attributes down #4040F
  ADD HL,DE         ; Add DE (31) to HL - move back to the right edge of the screen #405F
  RLA               ; rotate left accumulator outcome: move the left most pixel on to the carry flag

  LD B,$20          ; Loop 32 times
L7E1B: 
  LD A,(HL)         ; load A with what's at HL ie a row of 8 pixels at the right of the screen
  RLA               ; rotate left accumulator - puts the pixel from the left of screen, on the right of the screen, and moves pixels in the Attr 1 to the left and so on 
  LD (HL),A         ; draw it on the screen.
  DEC HL            ; hl = hl -1 : #405F - 1 = #405E. i.e., move one attribute to the left.
  DJNZ L7E1B        ; B=b-1. If B <>0 go to L7e1B ie do this 32 times

  POP HL        ; from the call earlier HL is right edge, 3 attr down: #405F
  INC H         ; next line of char
  DEC C         ; c=c-1 (C was 8, so for each of the 8 rows of pixels)
  JR NZ,L7E0F       ; If C is not 0 loop.
  RET         ; Return
  
START:
  LD HL,$405F ; Black boats - top 3rd of screen, right edge, 3rd attribute down
  CALL ScrLeft ;scroll left

  LD HL,$405F ; top 3rd of screen, right edge, 3rd attribute down
  CALL ScrLeft ;scroll left

  LD HL,$4080 ; Red crocs - top 3rd of screen, left edge, 5th attribute down
  CALL ScrRight ; scroll right

  LD HL,$4080  ; top 3rd of screen, left edge, 5th attribute down
  CALL ScrRight ; scroll right

  LD HL,$40DF  ; Blue boats - top 3rd, right edge, 7 from top
  CALL ScrLeft ; ;scroll left

  LD HL,$4800 ; Black crocs top of middle, left edge, 1st attribute
  CALL ScrRight ; scroll right

;The spider changes direction every 10 seconds This code uses the frames counter system variable to regulate this
  NOP
  NOP
  NOP
  NOP
  NOP
  NOP
  LD A,($5C79)  ; FRAMES counter - A system variable, Incremented every 20ms.
  NOP
  NOP
  NOP
  NOP
  NOP
  NOP
  NOP
  AND $02
  JR Z,SpiderL

;scroll the spider right 3 times

  LD HL,$4840
  CALL ScrRight

  LD HL,$4840
  CALL ScrRight

  LD HL,$4840
  CALL ScrRight

  JR Traffic

SpiderL:
;scroll the spider left
  LD HL,$485F 
  CALL ScrLeft

  LD HL,$485F
  CALL ScrLeft

  LD HL,$485F
  CALL ScrLeft

Traffic:

  LD HL,$4880 ; magenta  cars
  CALL ScrRight

  LD HL,$48C0 ; Blue cars
  CALL ScrRight

  LD HL,$48C0
  CALL ScrRight

  LD HL,$501F ; green cars 
  CALL ScrLeft

  LD HL,$501F
  CALL ScrLeft

  LD HL,$505F ; yellow cars 
  CALL ScrLeft
Speed1:
RET ; to speed up the game remove these RET commands with a poke 

end asm
END function


Function fastcall SpeedC1()
ASM
  LD HL,$4880
  CALL ScrRight

  LD HL,$48C0 ; blue cars
  CALL ScrRight

  LD HL,$501F
  CALL ScrLeft

  LD HL,$505F
  CALL ScrLeft
RET
end asm
END function

Function fastcall SpeedC2()
ASM

  LD HL,$405F ; yellow card
  CALL ScrLeft

  LD HL,$4080 ; red crocks
  CALL ScrRight

  LD HL,$4800 ;magenta cars
  CALL ScrRight

RET
end asm
END function

Function fastcall SpeedC3()
ASM

  LD HL,$405F     ; yellow car
  CALL ScrLeft

  LD HL,$40DF ; blue boats
  CALL ScrLeft

  LD HL,$4880   ;magenta cars
  CALL ScrRight

  LD HL,$48C0 ; blue cars
  CALL ScrRight

  RET

RET
end asm
END function

SUB sound(soundNum as uByte)
asm
#include "BeepFXPlayer.asm"
#include "SoundData.asm"

SoundEnd:
pop IX

end asm
end sub